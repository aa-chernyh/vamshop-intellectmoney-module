#Модуль оплаты платежной системы IntellectMoney для CMS VamShop

> **Внимание!** <br>
Данная версия актуальна на *9 июля 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/VamShop#whatnew.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/VamShop#557863e94642328ca24724af3ef6121bcdc5f9
