<?php

$userSettingsPath = DIR_FS_CATALOG_MODULES . 'payment/IntellectMoneyCommon/UserSettings.php';
$languageHelperPath = DIR_FS_CATALOG_MODULES . 'payment/IntellectMoneyCommon/LanguageHelper.php';

include_once $userSettingsPath;
include_once $languageHelperPath;

define('MODULE_PAYMENT_INTELLECTMONEY_TEXT_TITLE', 'IntellectMoney');
define('MODULE_PAYMENT_INTELLECTMONEY_TEXT_PUBLIC_TITLE', 'IntellectMoney');
define('MODULE_PAYMENT_INTELLECTMONEY_TEXT_ADMIN_DESCRIPTION', 'Модуль оплаты IntellectMoney.');
define('MODULE_PAYMENT_INTELLECTMONEY_TEXT_DESCRIPTION', 'После нажатия кнопки Подтвердить заказ Вы перейдёте на сайт платёжной системы для оплаты заказа, после оплаты Ваш заказ будет выполнен.');
define('MODULE_PAYMENT_INTELLECTMONEY_STATUS_TITLE', 'Разрешить модуль IntellectMoney');
define('MODULE_PAYMENT_INTELLECTMONEY_STATUS_DESC', 'Вы хотите разрешить использование модуля при оформлении заказов?');
define('MODULE_PAYMENT_INTELLECTMONEY_ALLOWED_TITLE', 'Разрешённые страны');
define('MODULE_PAYMENT_INTELLECTMONEY_ALLOWED_DESC', 'Укажите коды стран, для которых будет доступен данный модуль, например RU,DE (оставьте поле пустым, если хотите что б модуль был доступен покупателям из любых стран)');
define('MODULE_PAYMENT_INTELLECTMONEY_SORT_ORDER_TITLE', 'Порядок сортировки');
define('MODULE_PAYMENT_INTELLECTMONEY_SORT_ORDER_DESC', 'Порядок сортировки модуля.');
define('MODULE_PAYMENT_INTELLECTMONEY_ZONE_TITLE', 'Зона');
define('MODULE_PAYMENT_INTELLECTMONEY_ZONE_DESC', 'Если выбрана зона, то данный модуль оплаты будет виден только покупателям из выбранной зоны.');

//Константы like DIR_FS_CATALOG_MODULES не подгружаются на странице checkout.php, поэтому делаем проверку
if (file_exists($userSettingsPath) && file_exists($languageHelperPath)) {
    $userSettings = \PaySystem\UserSettings::getInstance();
    $languageHelper = \PaySystem\LanguageHelper::getInstance('ru');
    foreach ($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
        $cmsParamName = 'MODULE_PAYMENT_INTELLECTMONEY_' . strtoupper($paramName);
        define($cmsParamName . '_TITLE', $languageHelper->getTitle($paramName));
        define($cmsParamName . '_DESC', $languageHelper->getDesc($paramName));
    }
}
?>