<?php

/*
 * Если кто-либо будет вносить правки в этот модуль, знайте, что при любой ошибке в коде:
 *      1. отваливается страница выставления счета checkout.php
 *      2. админка перестает грузиться на моменте подгрузки файла модуля
 */

//error_reporting(E_ALL);
//ini_set("display_errors", 1);

require_once 'IntellectMoneyCommon/UserSettings.php';
require_once 'IntellectMoneyCommon/VATs.php';
require_once 'IntellectMoneyCommon/LanguageHelper.php';
require_once 'IntellectMoneyCommon/Order.php';
require_once 'IntellectMoneyCommon/Customer.php';
require_once 'IntellectMoneyCommon/Payment.php';
require_once 'IntellectMoneyCommon/Result.php';
//Именно include. Файл в админке грузится дважды, хоть он там совсем не нужен.
include 'intellectmoney_confirmation.php';

class intellectmoney {

    public $code, $title, $description, $enabled;
    private $paramPrefix = 'MODULE_PAYMENT_INTELLECTMONEY_';

    public function intellectmoney() {
        $this->title = constant($this->paramPrefix . 'TEXT_TITLE');
        $this->public_title = constant($this->paramPrefix . 'TEXT_PUBLIC_TITLE');

        global $order;
        if (is_object($order)) {
            $this->update_status();
        }

        $this->sort_order = constant($this->paramPrefix . 'SORT_ORDER');
        $this->code = 'intellectmoney';
        $this->icon = DIR_WS_ICONS . 'intellectmoney.png';
        $this->enabled = ((constant($this->paramPrefix . 'STATUS') == 'True') ? true : false);
        $this->description = constant($this->paramPrefix . 'TEXT_ADMIN_DESCRIPTION');
        $this->form_action_url = 'https://merchant.intellectmoney.ru/ru/';
    }

    //Срабатывает при выборе модуля на странице оформления заказа checkout.php
    public function selection() {
        if (vam_not_null($this->icon))
            $icon = vam_image($this->icon, $this->title);

        return array(
            'id' => $this->code,
            'icon' => $icon
        );
    }

    //Выставление счета
    public function process_button() {
        global $order;
        $orderId = explode('-', $_SESSION['cart_intellectmoney_id'])[1];
        $userSettings = $this->loadUserSettings();
        $customer = \PaySystem\Customer::getInstance($order->customer['email_address'], $order->customer['firstname'] . " " . $order->customer['lastname'], $order->customer['telephone']);
        $imOrder = \PaySystem\Order::getInstance(NULL, $orderId, $order->info['total'], $order->info['total'], NULL, NULL, $order->info['currency'], NULL, NULL);
        //products
        foreach ($order->products as $product) {
            $imOrder->addItem($product['price'], $product['qty'], $product['name'], $userSettings->getTax());
        }
        //delivery
        if ($order->info['shipping_cost'] > 0) {
            $imOrder->addItem($order->info['shipping_cost'], 1, $order->info['shipping_method'], $userSettings->getDeliveryTax());
        }

        $payment = \PaySystem\Payment::getInstance($userSettings, $imOrder, $customer);
        $processButtonString = "";
        foreach ($payment->getParamsForOrganization() as $paramName => $paramValue) {
            $processButtonString .= vam_draw_hidden_field($paramName, $paramValue);
        }

        $_SESSION['cart']->reset();
        return $processButtonString;
    }
    
    public function callback() {
        $order = new order($_REQUEST['orderId']);
        $userSettings = $this->loadUserSettings();

        $imParams = $this->getImParams($_REQUEST['orderId']);
        $imOrder = \PaySystem\Order::getInstance($imParams['invoiceId'], $_REQUEST['orderId'], $order->info['total_value'], $order->info['total_value'], NULL, NULL, $order->info['currency'], NULL, $imParams['lastImStatus']);
        $result = \PaySystem\Result::getInstance($_REQUEST, $userSettings, $imOrder, 'en');

        $response = $result->processingResponse();
        if ($response->changeStatusResult) {
            $this->saveImParams($_REQUEST['orderId'], $_REQUEST['paymentStatus'], $_REQUEST['paymentId']);
            $this->changeOrderStatus($_REQUEST['orderId'], $response->statusCMS);
        }

        echo $result->getMessage();
        die;
    }

    public function install() {
        $vatNamesLikeString = $this->getVatNamesLikeString();
        $sortNumber = 3;
        foreach ($this->keys() as $paramName) {
            $setFunction = "NULL";
            $useFunction = "NULL";
            switch ($paramName) {
                case $this->paramPrefix . "STATUS":
                case $this->paramPrefix . "HOLDMODE":
                case $this->paramPrefix . "TESTMODE":
                    $setFunction = "'vam_cfg_select_option(array(\\'True\\', \\'False\\'), '";
                    break;
                case $this->paramPrefix . "ZONE":
                    $setFunction = "'vam_cfg_pull_down_zone_classes('"; //Это не ошибка. Так надо
                    $useFunction = "'vam_get_zone_class_title'";
                    break;
                case $this->paramPrefix . "STATUSCREATED":
                case $this->paramPrefix . "STATUSPAID":
                case $this->paramPrefix . "STATUSHOLDED":
                case $this->paramPrefix . "STATUSCANCELLED":
                case $this->paramPrefix . "STATUSPARTIALLYPAID":
                case $this->paramPrefix . "STATUSREFUNDED":
                    $setFunction = "'vam_cfg_pull_down_order_statuses('";
                    $useFunction = "'vam_get_order_status_name'";
                    break;
                case $this->paramPrefix . "TAX":
                    $setFunction = "'vam_cfg_select_option($vatNamesLikeString, \\'\\', \\'" . $this->paramPrefix . "TAX\\', '";
                    break;
                case $this->paramPrefix . "DELIVERYTAX":
                    $setFunction = "'vam_cfg_select_option($vatNamesLikeString, \\'\\', \\'" . $this->paramPrefix . "DELIVERYTAX\\', '";
                    break;
                default:
                    break;
            }

            $this->installInsertQuery($paramName, $sortNumber, $setFunction, $useFunction);
            $sortNumber++;
        }
    }

    public function remove() {
        vam_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    //Используется в админке для открытия настроек
    public function keys() {
        $userSettings = \PaySystem\UserSettings::getInstance();
        $adminParams = array();
        foreach ($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $adminParams[] = $this->paramPrefix . strtoupper($paramName);
        }
        return array_merge(array($this->paramPrefix . 'STATUS', $this->paramPrefix . 'ALLOWED', $this->paramPrefix . 'SORT_ORDER', $this->paramPrefix . 'ZONE'), $adminParams);
    }

    public function after_process() {
        return false;
    }

    public function output_error() {
        return false;
    }

    function javascript_validation() {
        return false;
    }

    //Check статуса (включен/выключен)
    public function check() {
        if (!isset($this->_check)) {
            $check_query = vam_db_query("select configuration_value from " . TABLE_CONFIGURATION . " where configuration_key = '" . $this->paramPrefix . "STATUS'");
            $this->_check = vam_db_num_rows($check_query);
        }
        return $this->_check;
    }

    //Геозона
    public function update_status() {
        global $order;

        if (($this->enabled == true) && ((int) constant($this->paramPrefix . 'ZONE') > 0)) {
            $check_flag = false;
            $check_query = vam_db_query("select zone_id from " . TABLE_ZONES_TO_GEO_ZONES . " where geo_zone_id = '" . constant($this->paramPrefix . 'ZONE') . "' and zone_country_id = '" . $order->billing['country']['id'] . "' order by zone_id");
            while ($check = vam_db_fetch_array($check_query)) {
                if ($check['zone_id'] < 1) {
                    $check_flag = true;
                    break;
                } elseif ($check['zone_id'] == $order->billing['zone_id']) {
                    $check_flag = true;
                    break;
                }
            }

            if ($check_flag == false) {
                $this->enabled = false;
            }
        }
    }

    public function pre_confirmation_check() {
        if (empty($_SESSION['cart']->cartID)) {
            $_SESSION['cartID'] = $_SESSION['cart']->cartID = $_SESSION['cart']->generate_cart_id();
        }

        if (!isset($_SESSION['cartID'])) {
            $_SESSION['cartID'] = $_SESSION['cart']->generate_cart_id();
        }
    }

    //Срабатывает после отправки формы
    public function confirmation() {
        imConfirmation();
        return array('title' => constant($this->paramPrefix . "TEXT_DESCRIPTION"));
    }

    private function getVatNamesLikeString() {
        $vatNamesArrayString = "array(";
        $vatList = \PaySystem\VATs::getList();
        $iterator = 0;
        foreach ($vatList as $vat) {
            $iterator++;
            $vatNamesArrayString .= "\\'" . $vat['name'] . "\\'";
            if (count($vatList) != $iterator) {
                $vatNamesArrayString .= ", ";
            }
        }

        return $vatNamesArrayString .= ")";
    }

    private function installInsertQuery($paramName, $sortNumber, $setFunction = 'NULL', $useFunction = 'NULL') {
        vam_db_query("
            insert into " . TABLE_CONFIGURATION . " (configuration_key, configuration_value, configuration_group_id, sort_order, date_added, set_function, use_function) "
                . "values ('$paramName', '', '6', '$sortNumber', now(), $setFunction, $useFunction)
        ");
    }

    private function loadUserSettings() {
        \PaySystem\LanguageHelper::getInstance('ru');
        $userSettings = \PaySystem\UserSettings::getInstance();
        $params = array();
        $VATs = $this->getVATsAsKeyValue();
        foreach ($userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $paramValue = constant($this->paramPrefix . strtoupper($paramName));
            if (in_array($paramName, array('holdMode', 'testMode'))) {
                $params[$paramName] = ($paramValue == 'True') ? 1 : 0;
            } elseif (stripos($paramName, "tax") !== false) {
                $params[$paramName] = array_search($paramValue, $VATs);
            } else {
                $params[$paramName] = $paramValue;
            }
        }
        $userSettings->setParams($params);
        return $userSettings;
    }

    private function getVATsAsKeyValue() {
        $VATs = array();
        foreach (\PaySystem\VATs::getList() as $value) {
            $VATs[$value['id']] = $value['name'];
        }
        return $VATs;
    }

    private function saveImParams($orderId, $lastImStatus = null, $invoiceId = null) {
        $params = $this->getImParams($orderId);

        if (!empty($lastImStatus)) {
            $params['lastImStatus'] = (int) $lastImStatus;
        }
        if (!empty($invoiceId)) {
            $params['invoiceId'] = (int) $invoiceId;
        }

        $sqlDataArray = array('comments' => json_encode($params));
        vam_db_perform(TABLE_ORDERS, $sqlDataArray, 'update', "orders_id='" . (int) $orderId . "'");
    }

    private function getImParams($orderId) {
        $notesDbObject = vam_db_query("select comments from " . TABLE_ORDERS . " where orders_id = '" . (int) $orderId . "' LIMIT 1");
        $notes = vam_db_fetch_array($notesDbObject)['comments'];

        if (!empty($notes)) {
            return (array) json_decode($notes);
        }

        return array();
    }
    
    private function changeOrderStatus($orderId, $statusCMS) {
        $sqlDataArray = array('orders_status' => $statusCMS);
        vam_db_perform('orders', $sqlDataArray, 'update', "orders_id='" . (int) $orderId . "'");

        $sqlDataArray = array(
            'orders_id' => (int) $orderId,
            'orders_status_id' => $statusCMS,
            'date_added' => 'now()',
            'customer_notified' => '0',
            'comments' => 'IntellectMoney accepted this order payment'
        );
        vam_db_perform('orders_status_history', $sqlDataArray);
    }

}

?>